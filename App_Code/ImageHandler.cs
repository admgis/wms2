using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mime;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

/// <summary>
/// Summary description for ImageHandler
/// </summary>
public class ImageHandler : IHttpHandler
{
    public ImageHandler()
    {
    }

    public string GetContentType(String path)
    {
        switch (Path.GetExtension(path))
        {
            case ".bmp": return "Image/bmp";
            case ".gif": return "Image/gif";
            case ".jpg": return "Image/jpeg";
            case ".png": return "Image/png";
            default: break;
        }
        return String.Empty;
    }

    public ImageFormat GetImageFormat(String tipo)
    {
        switch (tipo)
        {
            case "image/bmp": return ImageFormat.Bmp;
            case "image/gif": return ImageFormat.Gif;
            case "image/jpg": return ImageFormat.Jpeg;
            case "image/png": return ImageFormat.Png;                
            default: return null;
        }
    }

    public byte[] WatermarkImage(byte[] imagenEntrada, string tipoImagen)
    {

        byte[] imageBytes = null;
        //if (File.Exists(ficheroEntrada))
        //{
            // Normally you'd put this in a config file somewhere.
        string watermark = System.Configuration.ConfigurationManager.AppSettings.Get("MensajeEscalaMinima");
        
            Stream s = new MemoryStream(imagenEntrada);
            Image image = Image.FromStream(s);
            //Image image = Image.FromFile(ficheroEntrada);

            Graphics graphic;
            if (image.PixelFormat != PixelFormat.Indexed && image.PixelFormat != PixelFormat.Format8bppIndexed && image.PixelFormat != PixelFormat.Format4bppIndexed && image.PixelFormat != PixelFormat.Format1bppIndexed)
            {
                // Graphic is not a Indexed (GIF) image
                graphic = Graphics.FromImage(image);                
            }
            else
            {
                /* Cannot create a graphics object from an indexed (GIF) image. 
                 * So we're going to copy the image into a new bitmap so 
                 * we can work with it. */
                Bitmap indexedImage = new Bitmap(image);
                graphic = Graphics.FromImage(indexedImage);

                // Draw the contents of the original bitmap onto the new bitmap. 
                graphic.DrawImage(image, 0,0, image.Width, image.Height);
                image = indexedImage;
            }
            graphic.SmoothingMode = SmoothingMode.AntiAlias & SmoothingMode.HighQuality;

            Font myFont = new Font("Arial", 8);
            SolidBrush brush = new SolidBrush(Color.FromArgb(255, Color.Red));

            /* This gets the size of the graphic so we can determine 
             * the loop counts and placement of the watermarked text. */
            SizeF textSize = graphic.MeasureString(watermark, myFont);

            //////// Write the text across the image. 
            //////for (int y = 0; y < image.Height; y++)
            //////{
            //////    for (int x = 0; x < image.Width; x++)
            //////    {
            //////        PointF pointF = new PointF(x, y);
            //////        graphic.DrawString(watermark, myFont, brush, pointF);
            //////        x += Convert.ToInt32(textSize.Width);
            //////    }
            //////    y += Convert.ToInt32(textSize.Height);
            //////}
            //Escribimos s�lo en el centro de la imagen
            int x, y;
            x = Convert.ToInt32((image.Width/2) - (textSize.Width/2));
            y = Convert.ToInt32((image.Height/2) - (textSize.Height/2))-10;

            PointF pointF = new PointF(x, y);            
            graphic.DrawString(watermark, myFont, brush, pointF);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                
                image.Save(memoryStream, GetImageFormat(tipoImagen));
                imageBytes = memoryStream.ToArray();
            }

        //}
        return imageBytes;
    }

    public byte[] CrearImagenBlanco(int width, int height)
    {
        byte[] imageBytes = null;

       using (Bitmap image = new Bitmap(width, height, PixelFormat.Format32bppArgb))
        {
            Graphics g = Graphics.FromImage((Image)image);
            g.TranslateTransform(image.Width, image.Height);
            g.RotateTransform(180.0F); //note that we need the rotation as the default is down

            // draw text
            //g.DrawString(text, f, Brushes.Black, 0f, 0f, stringFormat);
            using (MemoryStream memStream = new MemoryStream())
            {
                //note that context.Response.OutputStream doesn't support the Save, but does support WriteTo
                image.Save(memStream, ImageFormat.Png);
                imageBytes = memStream.ToArray();
            }
        }

        return imageBytes;
    }


    public byte[] WatermarkImage2(byte[] imagenEntrada, string tipoImagen,string pathImagen)
    {

        byte[] imageBytes = null;
        //if (File.Exists(ficheroEntrada))
        //{
        // Normally you'd put this in a config file somewhere.
               
        string watermark = "MARM - � Ministerio de Medio Ambiente y Medio Rural y Marino";
        Stream s = new MemoryStream(imagenEntrada);
        Image image = Image.FromStream(s);
        //Image image = Image.FromFile(ficheroEntrada);

        Graphics graphic;
        if (image.PixelFormat != PixelFormat.Indexed && image.PixelFormat != PixelFormat.Format8bppIndexed && image.PixelFormat != PixelFormat.Format4bppIndexed && image.PixelFormat != PixelFormat.Format1bppIndexed)
        {
            // Graphic is not a Indexed (GIF) image
            graphic = Graphics.FromImage(image);
        }
        else
        {
            /* Cannot create a graphics object from an indexed (GIF) image. 
             * So we're going to copy the image into a new bitmap so 
             * we can work with it. */
            Bitmap indexedImage = new Bitmap(image);
            graphic = Graphics.FromImage(indexedImage);

            // Draw the contents of the original bitmap onto the new bitmap. 
            graphic.DrawImage(image, 0, 0, image.Width, image.Height);
            image = indexedImage;
        }
        graphic.SmoothingMode = SmoothingMode.AntiAlias & SmoothingMode.HighQuality;

        Font myFont = new Font("Arial", 15);
        SolidBrush brush = new SolidBrush(Color.FromArgb(80, Color.White));

        /* This gets the size of the graphic so we can determine 
         * the loop counts and placement of the watermarked text. */
        SizeF textSize = graphic.MeasureString(watermark, myFont);

        //////// Write the text across the image. 
        //////for (int y = 0; y < image.Height; y++)
        //////{
        //////    for (int x = 0; x < image.Width; x++)
        //////    {
        //////        PointF pointF = new PointF(x, y);
        //////        graphic.DrawString(watermark, myFont, brush, pointF);
        //////        x += Convert.ToInt32(textSize.Width);
        //////    }
        //////    y += Convert.ToInt32(textSize.Height);
        //////}
        //Escribimos s�lo en el centro de la imagen

        // arg#2729: se convierte la marca de agua en opcional
        // quit�ndose si el valor pathWatermark del config est� vac�o o no existe.
        if (!String.IsNullOrEmpty(pathImagen))
        {
            Image imgLogo = Image.FromFile(pathImagen);
            int x, y;
            x = Convert.ToInt32((image.Width / 2) - (imgLogo.Width / 2));
            y = Convert.ToInt32((image.Height / 2) - (imgLogo.Height / 2));
            PointF pointF = new PointF(x, y);
            graphic.DrawImageUnscaled(imgLogo, x, y, imgLogo.Width, imgLogo.Height);
        }
        //Image imgLogo = Image.FromFile(pathImagen);
        //int x, y;
        //x = Convert.ToInt32((image.Width / 2) - (imgLogo.Width  / 2));
        //y = Convert.ToInt32((image.Height / 2) - (imgLogo.Height / 2));
        //PointF pointF = new PointF(x, y);
        //graphic.DrawImageUnscaled(imgLogo,x,y,imgLogo.Width,imgLogo.Height);
        //graphic.DrawString(watermark, myFont, brush, pointF);
        using (MemoryStream memoryStream = new MemoryStream())
        {
            image.Save(memoryStream, GetImageFormat(tipoImagen));
            imageBytes = memoryStream.ToArray();
        }

        //}
        return imageBytes;
    }

    #region IHttpHandler Members

    public bool IsReusable
    {
        get { return false; }
    }

    public void ProcessRequest(HttpContext context)
    {
        context.Response.Clear();
        context.Response.ContentType = GetContentType(context.Request.PhysicalPath);
        byte[] imageBytes = null;// WatermarkImage(context);
        if (imageBytes != null)
        {
            context.Response.OutputStream.Write(imageBytes, 0, imageBytes.Length);
        }
        else
        {
            // No bytes = no image which equals NO FILE. :)  
            // Therefore send a 404 - not found response. 
            context.Response.StatusCode = 404;
        }
        context.Response.End();
    }

    #endregion
}
