﻿using System;
using System.Data;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.OracleClient;
using System.Text.RegularExpressions;
using System.Linq;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Server;
using ESRI.ArcGIS.ADF;
using ESRI.ArcGIS.Geometry;
using System.Diagnostics;
using System.Collections;
using System.IO;
using GotDotNet.XInclude;
using System.Xml;


public partial class _Default : System.Web.UI.Page
{
    private ESRI.ArcGIS.ADF.ComReleaser _ComReleaser;
    private ESRI.ArcGIS.ADF.Connection.AGS.AGSServerConnection m_connection = null;
    private ESRI.ArcGIS.Server.IServerObjectManager m_som;
    private ESRI.ArcGIS.Server.IServerContext m_mapServerContext;
    private ESRI.ArcGIS.Server.IServerObjectExtensionManager m_soem;
    private ESRI.ArcGIS.Server.IServerObjectExtension m_soe;
    private ESRI.ArcGIS.Carto.IWMSServer m_wmsService;
    private IMapServer m_MapServer;
    private IServerObject m_ServerObject;
    private double escalaMax = 16000000;

    private static string RUTA_TRAZAS = System.Configuration.ConfigurationManager.AppSettings["RutaTrazas"];

    string[] separadorGrupos = new string[] { "|" };
    string[] separadorCapas = new string[] { "," };

    #region GoogleAnalytics

    private string getIP(HttpContext c)
    {
        string ips = c.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (!string.IsNullOrEmpty(ips))
        {
            return ips.Split(',')[0];
        }
        return c.Request.ServerVariables["REMOTE_ADDR"];
    }

    private string getCID()
    {
        string cid = Int32.Parse((new Random()).Next().ToString()) +
                    "." + (DateTime.Now.Millisecond);

        return cid;
    }

    // arg 2016-11-03: eventos de estadística - los gestionamos a través de un diccionario interno
    // http://www.carballude.es/blog/2012/01/21/c-los-peligros-de-las-variables-estaticas-en-librerias/comment-page-1/
    // static readonly porque actúa como una constante que queremos que al compilar se actualice
    private static readonly Dictionary<string, string> statsEventsDict = new Dictionary<string, string>()
	{
        // operaciones SLD
        {"GETLEGENDGRAPHIC", "Pedir Leyenda"},
        {"GETSTYLES", "Pedir Estilos"},
        // Versión 1.0.0
        {"MAP", "Cambiar Extensión"},
        {"FEATURE_INFO", "Identificar"},
        // Versiones posteriores
	    {"GETMAP", "Cambiar Extensión"},
        {"GETFEATUREINFO", "Identificar"}
	};

    // arg 2017-05-29 se solicitó que el getsld estuviera fuera de las peticiones
    // registradas por estadísticas.
    /// <summary>
    /// Lista de operaciones WMS excluídas del registro de estadísticas.
    /// </summary>
    private static readonly string[] statsExceptionsArray = new string[]{
        /*"CAPABILITIES", "GETCAPABILITIES",*/ "GETSLD"
    };

    private void sendGoogleAnalytics(string pageName, string eventValue = null)
    {
        var postData = "v=1";              // Version.
        postData += "&tid=" + System.Configuration.ConfigurationManager.AppSettings["TrackingID"];  // Tracking ID / Property ID.

        if (Request.Cookies != null)
        {
            if (Request.Cookies["_ga"] != null)
            {
                string cookieGA = Request.Cookies["_ga"].Value;
                if (cookieGA != null)
                {
                    string[] splitCookie = cookieGA.Split(new char[] { '.' });
                    // 2016-11-07 arg: daba un fallo si generábamos nosotros el CID porque se recuperaba un CID que, a lo sumo,
                    // tenía 2 elementos en el array e impedía el registro de estadísticas en algunos clientes
                    if (splitCookie.Length < 4)
                    {
                        postData += "&cid=" + cookieGA.ToString();     // recuperamos el cookieGA directamente
                    }
                    else
                    {
                        postData += "&cid=" + splitCookie[2] + "." + splitCookie[3];         // Anonymous Client ID.
                    }
                }
            }
            else
            {
                string cid = getCID();         // Anonymous Client ID.
                HttpCookie cook = new HttpCookie("_ga", cid);
                Response.Cookies.Add(cook);
                postData += "&cid=" + cid;
            }
        }
        else
        {
            string cid = getCID();         // Anonymous Client ID.
            HttpCookie cook = new HttpCookie("_ga", cid);
            Response.Cookies.Add(cook);
            postData += "&cid=" + cid;
        }

        // identificación del evento o página vista
        // string eventValue = null;
        // string operation = Request.QueryString["REQUEST"]; // da igual mayúsculas que minúsculas en la URL en ASP.NET
        // if (operation != null)
        // {
        //     statsEventsDict.TryGetValue(operation.ToUpper(), out eventValue);
        // }
        if (eventValue != null)
        {
            postData += "&t=event";     // Event hit type.
            postData += "&ec=WMS";
            postData += "&ea=" + eventValue;
        }
        else
        {
            postData += "&t=pageview";     // Pageview hit type.
        }

        postData += "&uip=" + getIP(HttpContext.Current);     // IP address override.
        postData += Request.ServerVariables["HTTP_USER_AGENT"];
        postData += "&dp=" + HttpUtility.UrlEncode(pageName);
        postData += "&dh=" + Request.Url.Host;

        var data = Encoding.ASCII.GetBytes(postData);

        var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://www.google-analytics.com/collect?" + postData);

        System.Net.WebProxy myproxy = new System.Net.WebProxy(ConfigurationManager.AppSettings.Get("proxyUrl"), int.Parse(ConfigurationManager.AppSettings.Get("proxyPort")));
        myproxy.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings.Get("proxyUser"), ConfigurationManager.AppSettings.Get("proxyPwd"));
        myproxy.BypassProxyOnLocal = false;
        request.Proxy = myproxy;
        request.Method = "POST";
        request.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), request);
    }

    private void GetRequestStreamCallback(IAsyncResult asynchronousResult)
    {
        System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)asynchronousResult.AsyncState;
        Stream postStream = request.EndGetRequestStream(asynchronousResult);
        postStream.Close();
        request.BeginGetResponse(new AsyncCallback(GetResponseCallback), request);
    }

    private void GetResponseCallback(IAsyncResult asynchronousResult)
    {
        System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)asynchronousResult.AsyncState;
        // End the operation
        System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.EndGetResponse(asynchronousResult);
        Stream streamResponse = response.GetResponseStream();
        StreamReader streamRead = new StreamReader(streamResponse);
        string responseString = streamRead.ReadToEnd();
        // Close the stream object
        streamResponse.Close();
        streamRead.Close();
        // Release the HttpWebResponse
        response.Close();
    }

    protected bool registrarPeticionParaEstadisticas()
    {
        var exceps = statsExceptionsArray;
        string reqStr = Request.QueryString["REQUEST"];
        if (String.IsNullOrEmpty(reqStr))
            return false;
        reqStr = reqStr.ToUpper();
        if (System.Array.IndexOf(exceps, reqStr) == -1)
        {
            try
            {
                // identificación del evento o página vista
                string eventValue = null;
                string operation = Request.QueryString["REQUEST"]; // da igual mayúsculas que minúsculas en la URL en ASP.NET
                if (String.IsNullOrEmpty(operation))
                {
                    operation = String.Empty;
                }
                statsEventsDict.TryGetValue(operation.ToUpper(), out eventValue);
                if (eventValue != null)
                {
                    sendGoogleAnalytics(Request.Url.AbsolutePath, eventValue);
                }
                sendGoogleAnalytics(Request.Url.AbsolutePath);
                return true;
            }
            catch
            {
            }
        }
        return false;
    }

    #endregion

    #region Funciones gestión layers

    private string[] ObtenerCapasMXD(string[] arrLayersQueryString, string[] capasMXDConfig, string[] capasConfig)
    {
        string[] capasMXD = new string[arrLayersQueryString.Length];
        for (int i = 0; i < arrLayersQueryString.Length; i++)
        {
            int indiceConfig = Array.IndexOf(capasConfig, arrLayersQueryString[i]);
            capasMXD[i] = capasMXDConfig.ElementAt(indiceConfig);
        }
        return capasMXD;
    }

    private string[] ObtenerCapasIdentificacion(string[] arrLayersQueryString, string[] capasIdentificacionConfig, string[] capasConfig)
    {
        string[] capasIdentificacion = new string[arrLayersQueryString.Length];
        for (int i = 0; i < arrLayersQueryString.Length; i++)
        {
            int indiceConfig = Array.IndexOf(capasConfig, arrLayersQueryString[i]);
            capasIdentificacion[i] = capasIdentificacionConfig.ElementAt(indiceConfig);
        }
        return capasIdentificacion;
    }

    private string[] ObtenerTitulosMXD(string[] arrLayersQueryString, string[] titulosMXDConfig, string[] capasConfig, string[] capasMXDConfig)
    {
        string[] titulosMXD = new string[arrLayersQueryString.Length];
        for (int i = 0; i < arrLayersQueryString.Length; i++)
        {
            int indiceConfig = Array.IndexOf(capasConfig, arrLayersQueryString[i]);
            if (titulosMXDConfig.Length > 0)
            {
                titulosMXD[i] = titulosMXDConfig.ElementAt(indiceConfig);
            }
            else
            {
                titulosMXD[i] = capasMXDConfig.ElementAt(indiceConfig);
            }
        }
        return titulosMXD;
    }
    private string[] ObtenerTituloGrupo(string[] arrLayersQueryString, string[] titulosConfig, string[] capasConfig)
    {
        string[] tituloGrupo = new string[arrLayersQueryString.Length];
        for (int i = 0; i < arrLayersQueryString.Length; i++)
        {
            int indiceConfig = Array.IndexOf(capasConfig, arrLayersQueryString[i]);
            tituloGrupo[i] = titulosConfig.ElementAt(indiceConfig);
        }
        return tituloGrupo;
    }

    private string[] ObtenerCapasPeticion(string[] capasMXD, string[] capasIdentificacion, string[] titulosMXD)
    {
        string[] capasMXDfil = new string[capasMXD.Length];
        for (int i = 0; i < capasMXD.Length; i++)
        {
            string[] capasMXDInd = capasMXD[i].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
            string[] capasIdentificacionInd = capasIdentificacion[i].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
            string[] titulosMXDInd = titulosMXD[i].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
            for (int j = 0; j < capasMXDInd.Length; j++)
            {
                if (capasIdentificacionInd[j] == "0")
                {
                    capasMXDInd[j] = "";
                    titulosMXDInd[j] = "";
                }
            }
            capasMXDfil[i] = string.Join(",", capasMXDInd);
            titulosMXD[i] = string.Join(",", titulosMXDInd);
            string[] capasMXDIndNew = capasMXDfil[i].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
            string[] titulosMXDIndNew = titulosMXD[i].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
            capasMXDfil[i] = string.Join(",", capasMXDIndNew);
            titulosMXD[i] = string.Join(",", titulosMXDIndNew);
        }
        ArrayList capasPeticionSalida = new ArrayList(capasMXDfil);
        if (capasPeticionSalida.IndexOf("") != -1)
        {
            capasPeticionSalida.RemoveAt(capasPeticionSalida.IndexOf(""));
        }
        String[] capasPeticion = (String[])capasPeticionSalida.ToArray(typeof(string));
        return capasPeticion;
    }

    private double calcularEscala(string SRS, double minX, double minY, double maxX, double maxY, double ancho, double alto, bool enAncho)
    {
        DateTime horaInicio = DateTime.Now;
        StringBuilder strBuilder = new StringBuilder();

        int sistemaRef;
        sistemaRef = int.Parse(SRS.Split(":".ToCharArray())[1]);

        bool usarMetodoManual = false;

        // Si es CRS:84 se convierte a EPSG:4326
        if (sistemaRef == 84)
            sistemaRef = 4326;

        // Se recupera el mapa del mxd
        IMapDescription mapDescription = m_MapServer.GetServerInfo(m_MapServer.DefaultMapName).DefaultMapDescription;
        _ComReleaser.ManageLifetime(mapDescription);
        int srsServidor = int.Parse(mapDescription.SpatialReference.FactoryCode.ToString());

        // Se carga el sistema de referencia solicitado
        ESRI.ArcGIS.Geometry.ISpatialReference srs = null;
        if (srsServidor != sistemaRef)
        {
            // Se crea un nuevo sistema de referencia para el extent
            ESRI.ArcGIS.Geometry.ISpatialReferenceFactory2 srEnv = m_mapServerContext.CreateObject("esriGeometry.SpatialReferenceEnvironment") as ESRI.ArcGIS.Geometry.ISpatialReferenceFactory2;
            srs = srEnv.CreateSpatialReference(sistemaRef);
        }
        else
        {
            srs = mapDescription.SpatialReference;
        }
        _ComReleaser.ManageLifetime(srs);

        double escala;
        if (!usarMetodoManual)
        {
            // Se crea el extent nuevo
            IMapExtent pMapExtent;
            pMapExtent = (IMapExtent)m_mapServerContext.CreateObject("esriCarto.MapExtent");
            _ComReleaser.ManageLifetime(pMapExtent);
            IEnvelope envelope = m_mapServerContext.CreateObject("esriGeometry.Envelope") as ESRI.ArcGIS.Geometry.IEnvelope;
            _ComReleaser.ManageLifetime(envelope);
            envelope.XMax = maxX;
            envelope.XMin = minX;
            envelope.YMax = maxY;
            envelope.YMin = minY;
            envelope.SpatialReference = srs;
            pMapExtent.Extent = envelope;
            mapDescription.SpatialReference = srs;
            mapDescription.MapArea = (IMapArea)pMapExtent;

            // Se cargan los parámetros de la imagen
            IImageDisplay mapDisplay = (IImageDisplay)m_mapServerContext.CreateObject("esriCarto.ImageDisplay");
            _ComReleaser.ManageLifetime(mapDisplay);
            mapDisplay.Height = int.Parse(alto.ToString());
            mapDisplay.Width = int.Parse(ancho.ToString());
            mapDisplay.DeviceResolution = 96;

            // Se hace el cálculo
            escala = m_MapServer.ComputeScale(mapDescription, mapDisplay);

        }
        else
        {
            // Creado, es igual de rápido que otro (quizás un poco más) pero más impreciso.
            double diffX = maxX - minX;
            double diffY = maxY - minY;
            int height = int.Parse(alto.ToString());
            int width = int.Parse(ancho.ToString());
            int resolution = 96; // ppp
            double mPerInch = 0.0254; // metros

            double escH = 0;
            double escV = 0;
            int escalaH = 0;
            int escalaV = 0;
            double unitsPerUnit = 0;
            if (srs is IProjectedCoordinateSystem)
            {
                // Sistema proyectado
                unitsPerUnit = (srs as IProjectedCoordinateSystem).CoordinateUnit.MetersPerUnit;
                // Escala horizontal
                escH = (diffX * unitsPerUnit) / (width * mPerInch / resolution);
                escalaH = int.Parse(Math.Truncate(escH).ToString());

                // Escala vertical
                escV = (diffY * unitsPerUnit) / (height * mPerInch / resolution);
                escalaV = int.Parse(Math.Truncate(escV).ToString());

                // Escala representativa
                escala = Math.Min(escalaH, escalaV);
            }
            else
            {
                // Sistema geográfico
                // Método por aproximación a la esfera local
                unitsPerUnit = (srs as IGeographicCoordinateSystem).CoordinateUnit.RadiansPerUnit; // conversión a radianes

                // Se calcula la latitud del punto central de la imagen y se pasa a radianes
                double latCentral = (maxY - minY) / 2 * unitsPerUnit;

                // Parámetros de cálculo
                double a = (srs as IGeographicCoordinateSystem).Datum.Spheroid.SemiMajorAxis; // Semieje mayor de la elipse meridiana
                double f = (srs as IGeographicCoordinateSystem).Datum.Spheroid.Flattening; // achatamiento
                double e2 = 2 * f - Math.Pow(f, 2);   // primera excentricidad al cuadrado en función del achatamiento
                double factor = Math.Pow(1 - e2 * Math.Pow(Math.Sin(latCentral), 2), 0.5);

                // Cálculo gran normal
                double N = a / factor;

                // Se calcula el radio de curvatura de la esfera local
                double numRho = a * (1 - e2);
                //double denRho = Math.Pow(Math.Pow(1 - e2 * Math.Pow(Math.Sin(latCentral), 2), 1.5), 0.5);
                double denRho = Math.Pow(factor, 1.5);
                double Rho = numRho / denRho;

                // Radio de la esfera local
                double radioT = Math.Pow(N * Rho, 0.5);

                // Calculo aproximado de la escala
                escala = Math.Truncate(diffY * unitsPerUnit * radioT / (height * mPerInch / resolution)); // Se coge la desviación en Y
            }
        }

        TimeSpan diff = DateTime.Now.Subtract(horaInicio);
        strBuilder.Append("Escala calculada: 1:" + Math.Round(escala, 0) + ". ");
        strBuilder.Append("Escala pedida en " + mapDescription.SpatialReference.FactoryCode.ToString() + ". ");
        strBuilder.Append(diff.TotalMilliseconds.ToString() + " ms");

        // arg 13/06/2014 Habilita la traza por web.config
        //bool habilitarTraza = true;
        //if (System.Configuration.ConfigurationManager.AppSettings["habilitarTraza"] != null)
            //habilitarTraza = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["PermitirSTYLES"]);
        //if (habilitarTraza)
        //Trazar(strBuilder.ToString()); //arg: genera un log para el cálculo de escalas (path: RutaTrazas en WebConfig)

        return escala;

    }

    public string[] obtenerTematico(int idTematico, string layerDefs)
    {
        string traza;
        traza = "";
        try
        {
            string strTablename, strDatasetName, strQuery, strResultado, strLeyenda;
            DataSet aDS;
            string[] colRes;
            colRes = new string[2];
            strResultado = "";
            strLeyenda = "";
            strTablename = "VIS_TMA_TEMATICOS";
            strDatasetName = "VIS_TMA_TEMATICOS";
            strQuery = "SELECT TMA_DS_RESULTADO,TMA_DS_LEYENDA FROM VIS_TMA_TEMATICOS WHERE SER_CO_ID=" + idTematico;
            traza = "Vamos a ejecutar query";
            //Trazar(traza);
            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName, out traza);
            foreach (DataRow aRow in aDS.Tables[0].Rows)
            {
                traza = "Ha devuelto datos!!!";
                //Trazar(traza);
                strResultado = aRow["TMA_DS_RESULTADO"] as string;
                strLeyenda = aRow["TMA_DS_LEYENDA"] as string;
            }
            colRes[0] = strResultado;
            colRes[1] = strLeyenda;
            //Cargamos lo obtenido en el contexto
            int layerAdded;


            IMapServerObjects2 mapServerObjects2 = (IMapServerObjects2)m_MapServer;
            this._ComReleaser.ManageLifetime(mapServerObjects2);
            string mapName = m_MapServer.DefaultMapName;
            traza = "Vamos a por el Get Map";
            //Trazar(traza);
            IMap map = mapServerObjects2.get_Map(mapName);
            this._ComReleaser.ManageLifetime(map);

            traza = "Hemos obtenido el mapa";
            //Trazar(traza);
            if (strLeyenda == null || strLeyenda == "")
            {
                IRasterLayer rLayer;
                //Ya lo tenemos generado previamente
                rLayer = m_mapServerContext.LoadObject(strResultado) as IRasterLayer;
                this._ComReleaser.ManageLifetime(rLayer);
                map.AddLayer(rLayer);
                mapServerObjects2.RefreshServerObjects();
                layerAdded = mapServerObjects2.get_LayerID(mapName, rLayer);
            }
            else
            {
                IFeatureLayer layer;
                //Ya lo tenemos generado previamente
                traza = "Vamos a LoadObject";
                //Trazar(traza);
                layer = m_mapServerContext.LoadObject(strResultado) as IFeatureLayer;
                this._ComReleaser.ManageLifetime(layer);

                if (layerDefs != null)
                {
                    //{"Playas":"PLY_CO_PLAYA=146"}
                    string[] arrLD = layerDefs.Split(new string[] { "\":\"" }, StringSplitOptions.RemoveEmptyEntries);
                    string defexpress = arrLD[1].Substring(0, arrLD[1].Length - 2);//2 últimos caracteres:"}
                    (layer as IFeatureLayerDefinition).DefinitionExpression = defexpress;
                }

                traza = "AddLayer";
                //Trazar(traza);
                map.ClearLayers();
                map.AddLayer(layer);
                mapServerObjects2.RefreshServerObjects();
                //if (layer != null)
                traza = "get_LayerID";
                //Trazar(traza);
                layerAdded = mapServerObjects2.get_LayerID(mapName, layer);
                //else
                //	layerAdded = mapServerObjects2.get_LayerID(mapName, iLy);
            }

            return colRes;
        }
        catch (Exception ex)
        {
            string[] colRes;
            colRes = new string[2];
            colRes[0] = ex.Message;
            return colRes;
        }

    }

    public void eliminarLayerTematico()
    {
        string idServicio = System.Configuration.ConfigurationManager.AppSettings.Get("IdServicio");

        if (idServicio != "")
        {
            if (m_MapServer != null)
            {
                IMapServerObjects2 mapServerObjects2 = (IMapServerObjects2)m_MapServer;
                _ComReleaser.ManageLifetime(mapServerObjects2);
                IMap map = mapServerObjects2.get_Map(m_MapServer.DefaultMapName);
                _ComReleaser.ManageLifetime(map);
                if (map.LayerCount > 0)
                {
                    map.ClearLayers();
                    mapServerObjects2.RefreshServerObjects();
                }
            }
        }
    }

    #endregion

    #region Utilidades

    private IWMSServer Conectar(out string traza)
    {
        traza = "";
        for (int i = 0; i < 10; i++)
        {
            try
            {
                string servidor = System.Configuration.ConfigurationManager.AppSettings.Get("ServidorArcGIS");
                string servicio = System.Configuration.ConfigurationManager.AppSettings.Get("ServicioArcGIS");
                traza = "Servicio no disponible en este momento. Disculpe las molestias.";
                //Trazar(traza);
                m_connection = new ESRI.ArcGIS.ADF.Connection.AGS.AGSServerConnection();
                this._ComReleaser.ManageLifetime(m_connection);
                m_connection.Host = servidor;
                m_connection.Connect(true);
                traza = "Ha conectado. Vamos a crear contexto";
                //Trazar(traza);
                m_som = m_connection.ServerObjectManager;
                this._ComReleaser.ManageLifetime(m_som);
                m_mapServerContext = m_som.CreateServerContext(servicio, "MapServer");
                traza = "Hemos creado contexto. A por server object";
                //Trazar(traza);
                this._ComReleaser.ManageLifetime(m_mapServerContext);
                m_ServerObject = m_mapServerContext.ServerObject;
                traza = "Tenemos server object. Vamos a por MapServer";
                //Trazar(traza);
                this._ComReleaser.ManageLifetime(m_ServerObject);
                m_MapServer = m_ServerObject as ESRI.ArcGIS.Carto.IMapServer;
                this._ComReleaser.ManageLifetime(m_MapServer);
                traza = "Tenemos MapServer. A por WMSServer";
                //Trazar(traza);
                m_soem = (ESRI.ArcGIS.Server.IServerObjectExtensionManager)m_mapServerContext.ServerObject;
                this._ComReleaser.ManageLifetime(m_soem);
                m_soe = m_soem.FindExtensionByTypeName("WMSServer");
                traza = "Tenemos WMSServer. A por el Servicio";
                //Trazar(traza);
                this._ComReleaser.ManageLifetime(m_soe);
                m_wmsService = (ESRI.ArcGIS.Carto.IWMSServer)m_soe;
                this._ComReleaser.ManageLifetime(m_wmsService);
                traza = "Tenemos Servicio";
                //Trazar(traza);
                break;
            }
            catch (Exception ex)
            {
                if (i < 10)
                {
                    //Log("Fallo " + i.ToString() + " contexto. Request: " + Request.QueryString.ToString() + "  " + ex.Message);
                    continue;
                }
                else
                {
                    //Log("Fallo " + i.ToString() + " contexto. Request: " + Request.QueryString.ToString()+ "  " + ex.Message);
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        return m_wmsService;

    }

    private void LiberarConexion()
    {
        if (m_mapServerContext != null)
        {
            m_mapServerContext.ReleaseContext();
        }
        if (this._ComReleaser != null)
        {
            _ComReleaser.Dispose();
        }
    }

    private static NameValueCollection ConstruirQueryString(string peticionURL)
    {
        NameValueCollection queryString = new NameValueCollection();

        // Elimina el ? del principio
        if (peticionURL.IndexOf("?") == 0)
            peticionURL = peticionURL.Remove(0, 1);

        // Se sustraen los parámetros como nombre/valor
        string[] pairs = peticionURL.Split("&".ToCharArray());
        foreach (string par in pairs)
        {
            // Se separan en dos
            string nombre = "", valor = "";
            var equalIndex = par.IndexOf("=");
            if (equalIndex > -1)
            {
                nombre = par.Substring(0, equalIndex);
            }
            if (!String.IsNullOrEmpty(nombre))
            {
                valor = par.Substring(equalIndex + 1);
                queryString.Add(nombre, valor);
            }
        }

        return queryString;
    }

    private string reconstruirStrQueryString(NameValueCollection queryString)
    {
        Array copyNames = Array.CreateInstance(typeof(string), queryString.Count);
        Array copyValues = Array.CreateInstance(typeof(string), queryString.Count);
        queryString.AllKeys.CopyTo(copyNames, 0);
        queryString.CopyTo(copyValues, 0);
        string strQueryString = "";
        string strSep = "";
        for (int iReq = 0; iReq < copyNames.Length; iReq++)
        {
            strQueryString += strSep + copyNames.GetValue(iReq).ToString() + "=" + copyValues.GetValue(iReq).ToString();
            strSep = "&";
        }
        return strQueryString;
    }

    private static string ExtraerQueryURL(string peticionURL)
    {
        // Se elimina la parte invariable de la petición y solo se devuelven los parámetros
        int indice1 = peticionURL.IndexOf("?");
        peticionURL = (indice1 != -1) ? peticionURL.Substring(indice1) : null;

        return peticionURL;
    }

    private DataSet ExecuteQuery(string strquery, string strTableName, string strDatasetName, out string traza)
    {
        traza = "";
        try
        {
            string strConn;
            traza = "Cogemos config";
            strConn = System.Configuration.ConfigurationManager.AppSettings.Get("bbdd");
            OracleConnection aConn = new OracleConnection(strConn);
            traza = "Abrimos conexión";
            aConn.Open();
            traza = "Conexión abierta";
            OracleDataAdapter mAdapter = new OracleDataAdapter();

            mAdapter.SelectCommand = new OracleCommand(strquery, aConn);

            DataSet aDataSet = new DataSet(strDatasetName);

            mAdapter.Fill(aDataSet, strTableName);

            aConn.Close();
            return aDataSet;
        }
        catch (Exception ex)
        {
            //agm: tratamos el error
            traza = "Hay error al recuperar";
            return null;
        }
    }

    // <summary>
    // Function to save byte array to a file
    // </summary>
    // <param name="_FileName">File name to save byte array</param>
    // <param name="_ByteArray">Byte array to save to external file</param>
    // <returns>Return true if byte array save successfully, if not return false</returns>
    public bool ByteArrayToFile(string _FileName, byte[] _ByteArray)
    {
        try
        {
            // Open file for reading
            System.IO.FileStream _FileStream = new System.IO.FileStream(_FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);

            // Writes a block of bytes to this stream using data from a byte array.
            _FileStream.Write(_ByteArray, 0, _ByteArray.Length);

            // close file stream
            _FileStream.Close();

            return true;
        }
        catch (Exception _Exception)
        {
            // Error
            Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
        }

        // error occured, return false
        return false;
    }

    private ESRI.ArcGIS.Geometry.ISpatialReference GenerarSistemaCoordenadas(int SRS)
    {
        ESRI.ArcGIS.Geometry.ISpatialReference res = null;
        ESRI.ArcGIS.Geometry.ISpatialReferenceFactory2 srEnv = m_mapServerContext.CreateObject("esriGeometry.SpatialReferenceEnvironment") as ESRI.ArcGIS.Geometry.ISpatialReferenceFactory2;
        _ComReleaser.ManageLifetime(srEnv);
        res = srEnv.CreateSpatialReference(SRS);
        _ComReleaser.ManageLifetime(res);
        return res;
    }

    private static void Log(string logMessage)
    {
        System.IO.StreamReader readerFichero;
        string nombreFichero = System.Configuration.ConfigurationManager.AppSettings["DirectorioTrabajo"] + "\\WmsLog_" + System.DateTime.Now.ToString("yyyyMMdd") + ".log";

        if (!System.IO.File.Exists(nombreFichero))
        {
            System.IO.FileStream w;
            w = System.IO.File.Create(nombreFichero);
            w.Close();
        }
        readerFichero = new System.IO.StreamReader(nombreFichero);
        string strTexto = readerFichero.ReadToEnd();
        readerFichero.Close();
        readerFichero.Dispose();
        strTexto = strTexto + logMessage + "    " + System.DateTime.Now.TimeOfDay + Environment.NewLine;
        System.IO.StreamWriter writerFichero = new System.IO.StreamWriter(nombreFichero);
        writerFichero.Write(strTexto);
        writerFichero.Flush();
        writerFichero.Close();
        writerFichero.Dispose();
    }

    public void Trazar(string mensaje)
    {
        System.IO.DirectoryInfo dirInfo = new System.IO.DirectoryInfo(RUTA_TRAZAS);
        if (dirInfo.Exists == false)
        {
            dirInfo.Create();
        }
        DateTime fecha = DateTime.Now;

        System.IO.FileStream fs = new System.IO.FileStream(RUTA_TRAZAS + "\\Traza" +
                        fecha.Year.ToString() + fecha.Month.ToString() + fecha.Day.ToString() + ".log",
                        FileMode.Append, FileAccess.Write,
                        FileShare.Write);

        System.IO.StreamWriter sw = null;
        try
        {
            sw = new StreamWriter(fs);

            string strMSeconds = "00" + fecha.Millisecond.ToString();
            strMSeconds = strMSeconds.Substring(strMSeconds.Length - 3, 3);

            sw.WriteLine("[" + fecha.ToLongTimeString() + "." + strMSeconds + "]" + mensaje);
            sw.Flush();
        }
        catch (Exception ex)
        {

        }
        finally
        {
            sw.Close();
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        bool estadisticasRegistradas = registrarPeticionParaEstadisticas();
        string trazaGeneral;
        trazaGeneral = "";
        string myPath = this.Page.MapPath(this.AppRelativeTemplateSourceDirectory);
        //string pathFicheroLog = System.Configuration.ConfigurationManager.AppSettings["pathLog"];
        string tolerancia = System.Configuration.ConfigurationManager.AppSettings["Tolerancia"];
        string idServicio = System.Configuration.ConfigurationManager.AppSettings.Get("IdServicio");

        try
        {
            NameValueCollection req;

            string strReq;

            // Problema de codificación
            // arg subsanación infame, pero funciona con todos los gis de escritorio probados y navegadores
            if (Request.Url.Query.Contains("%EF%BF%BD")) // Caracter incapaz de codificar
                strReq = Request.RawUrl.ToString();
            else
                strReq = Request.Url.ToString();

            strReq = HttpUtility.UrlDecode(strReq, Encoding.UTF8); // para udig FeatureInfo
            strReq = ExtraerQueryURL(strReq);

            Encoding encEntrada = Encoding.GetEncoding("UTF-8");
            string strPeticion = "";
            string post = "";
            string tipoSalida = "";
            string SLDstrEstilos = "";
            string SLDstrGrupos = "";
            string[] peticiones = null;
            string[] titulosMXD = null;
            byte[] respuesta = null;
            bool cambioFormato = false;

            req = ConstruirQueryString(strReq);
            strPeticion = req.Get("REQUEST");

            string stylesReq = req.Get("STYLES");
            string layersReq = req.Get("LAYERS");
            string layerReq = req.Get("LAYER");
            string queryLayersReq = req.Get("QUERY_LAYERS");

            #region GET CONFIG

            string[] capasConfig = System.Configuration.ConfigurationManager.AppSettings.Get("Capas").Split(separadorGrupos, StringSplitOptions.RemoveEmptyEntries);
            string[] capasMXDConfig = System.Configuration.ConfigurationManager.AppSettings.Get("CapasMXD").Split(separadorGrupos, StringSplitOptions.RemoveEmptyEntries);
            string[] capasIdentificacionConfig = System.Configuration.ConfigurationManager.AppSettings.Get("CapasIdentificables").Split(separadorGrupos, StringSplitOptions.RemoveEmptyEntries);
            string[] titulosConfig = System.Configuration.ConfigurationManager.AppSettings.Get("Titulos").Split(separadorGrupos, StringSplitOptions.RemoveEmptyEntries);
            string[] titulosMXDConfig = System.Configuration.ConfigurationManager.AppSettings.Get("TitulosMXD").Split(separadorGrupos, StringSplitOptions.RemoveEmptyEntries);

            string[] capasMXD = null;
            string[] capasIdentificacion = null;
            string[] capasPeticiones = null;

            string[] arrQueryLayersReq = null;
            string[] arrLayerReq = null;
            string[] arrLayersReq = null;

            if (layersReq != null)
            {
                arrLayersReq = layersReq.Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
            }
            if (queryLayersReq != null)
            {
                arrQueryLayersReq = queryLayersReq.Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
            }
            if (layerReq != null)
            {
                arrLayerReq = layerReq.Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
            }

            #endregion

            if (strPeticion.Equals("GETSLD", StringComparison.CurrentCultureIgnoreCase) || strPeticion.Equals("GETSTYLES", StringComparison.CurrentCultureIgnoreCase))
            {
                if (strReq.Contains("TRAZA"))
                {
                    Trazar("comunTematicosWMS " + strReq);
                }
                //string SLDrutaBase = this.Page.MapPath(this.AppRelativeTemplateSourceDirectory);
                string SLDpathEstilos = System.Configuration.ConfigurationManager.AppSettings.Get("PathEstilos");
                string SLDnombreFicheroEstilos = System.Configuration.ConfigurationManager.AppSettings.Get("NombreFicheroEstilos");
                string SLDgrupos = myPath + "/estilos/SLDgrupos.xml";
                XIncludingReader xir=null;
                XmlTextReader r =null;
                XIncludingReader xit = null;
                XmlTextReader t = null;
                try
                {
                    r = new XmlTextReader(SLDpathEstilos + SLDnombreFicheroEstilos);
                    xir = new XIncludingReader(r);
                    XmlDocument doc = new XmlDocument();
                    doc.Load(xir);
                    SLDstrEstilos = doc.InnerXml.ToString();
                    xir.Close();
                    // 20171128 @ADR: En caso de existir el nuevo SLDgrupos.xml (ver #3304) se procede a su lectura
                    if (strPeticion.Equals("GETSTYLES", StringComparison.CurrentCultureIgnoreCase) && File.Exists(SLDgrupos))
                    {
                        t = new XmlTextReader(SLDgrupos);
                        xit = new XIncludingReader(t);
                        XmlDocument doct = new XmlDocument();
                        doct.Load(xit);
                        SLDstrGrupos = doct.InnerXml.ToString();
                        xit.Close();
                    }
                    else if (strPeticion.Equals("GETSLD", StringComparison.CurrentCultureIgnoreCase))
                    {
                        Response.Clear();
                        Response.ContentType = tipoSalida;
                        string SLDnombreFicheroSalida = "attachment; filename=SLD.xml";
                        Response.AddHeader("content-disposition", SLDnombreFicheroSalida);
                        Response.Write(SLDstrEstilos);
                    }
                }
                catch (Exception ex)
                {
                    string strError = "<ServiceExceptionReport version=\"1.1.1\"><ServiceException code=\"InvalidFormat\"><![CDATA[ " + ex.Message + "  ]]></ServiceException></ServiceExceptionReport>";
                    Response.Clear();
                    Response.ContentType = "text/xml";
                    Response.Write(strError);
                    if (xir != null)
                        xir.Close();
                    return;
                }
            }

            if (strPeticion.Equals("GETFEATUREINFO", StringComparison.CurrentCultureIgnoreCase))
            {
                if (tolerancia != null)
                {
                    int intTolerancia = int.Parse(tolerancia);

                    if (intTolerancia != 1)
                    {

                        NameValueCollection req2 = new NameValueCollection(req);
                        if (req.Get("WIDTH") != null)
                        {
                            decimal width = decimal.Parse(req.Get("WIDTH"));
                            req2.Set("WIDTH", decimal.Round(width / intTolerancia, 0).ToString());

                        }
                        if (req.Get("HEIGHT") != null)
                        {
                            decimal height = decimal.Parse(req.Get("HEIGHT"));
                            req2.Set("HEIGHT", decimal.Round(height / intTolerancia, 0).ToString());
                        }
                        if (req.Get("I") != null)
                        {
                            decimal identI = decimal.Parse(req.Get("I"));
                            req2.Set("I", decimal.Round(identI / intTolerancia, 0).ToString());
                        }
                        if (req.Get("J") != null)
                        {
                            decimal identJ = decimal.Parse(req.Get("J"));
                            req2.Set("J", decimal.Round(identJ / intTolerancia, 0).ToString());
                        }
                        if (req.Get("X") != null)
                        {
                            decimal identX = decimal.Parse(req.Get("X"));
                            req2.Set("X", decimal.Round(identX / intTolerancia, 0).ToString());
                        }
                        if (req.Get("Y") != null)
                        {
                            decimal identY = decimal.Parse(req.Get("Y"));
                            req2.Set("Y", decimal.Round(identY / intTolerancia, 0).ToString());
                        }

                        req = req2;

                        Array copyNames = Array.CreateInstance(typeof(string), req.Count);
                        Array copyValues = Array.CreateInstance(typeof(string), req.Count);
                        req.AllKeys.CopyTo(copyNames, 0);
                        req.CopyTo(copyValues, 0);
                        strReq = "";
                        string strSep = "";
                        for (int iReq = 0; iReq < copyNames.Length; iReq++)
                        {
                            strReq += strSep + copyNames.GetValue(iReq).ToString() + "=" + copyValues.GetValue(iReq).ToString();
                            strSep = "&";
                        }
                    }
                }
            }

            else if (strPeticion.Equals("GetMap", StringComparison.CurrentCultureIgnoreCase) ||
                (strPeticion.Equals("Map", StringComparison.CurrentCultureIgnoreCase)))
            {
                capasMXD = ObtenerCapasMXD(arrLayersReq, capasMXDConfig, capasConfig);
                if (stylesReq != "")
                {
                    for (int k = 0; k < capasMXD.Length; k++)
                    {
                        string[] capasMXDsplit = capasMXD[k].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
                        string[] stylesMXDsplit = new string[capasMXDsplit.Length];
                        for (int j = 0; j < stylesMXDsplit.Length; j++)
                        {
                            stylesMXDsplit[j] = stylesReq;
                        }
                        string stylesNewReq = string.Join(",", stylesMXDsplit);
                        req.Set("STYLES", stylesNewReq);
                        strReq = reconstruirStrQueryString(req);
                    }
                }
                req.Set("LAYERS", capasMXD[0]);
                strReq = reconstruirStrQueryString(req);
                //Trazar("Esto es strReq tras GetMap: " + strReq);
            }

            switch (Request.HttpMethod)
            {
                case "GET":
                    if (strReq.Length > 0)
                    {
                        if (strReq.Substring(0, 1) == "?")
                        {
                            strReq = strReq.Substring(1);
                        }

                        strPeticion = req.Get("REQUEST");
                        if (strPeticion.Equals("GETFEATUREINFO", StringComparison.CurrentCultureIgnoreCase))
                        {
                            // Si se pide un formato text/html se cambia
                            if (strReq.Contains("text/html")) // arg añadido application/vnd.ogc.wms_xml porque no devolvía nada el servidor
                            {
                                req.Set("INFO_FORMAT", "application/vnd.esri.wms_featureinfo_xml");
                                strReq = reconstruirStrQueryString(req);
                                cambioFormato = true;
                            }
                        }
                    }

                    break;

                case "POST":
                    System.IO.StreamReader strReader;
                    strReader = new System.IO.StreamReader(Page.Request.InputStream, encEntrada);
                    post = strReader.ReadToEnd();
                    post = HttpUtility.UrlDecode(post, encEntrada); // UTF-8

                    if (post.ToUpper().Contains("GETMAP"))
                    {
                        strPeticion = "GETMAP";

                        int indice = post.ToUpper().IndexOf("STYLES");
                        indice = post.ToUpper().IndexOf("=", indice);
                        int indice2 = post.ToUpper().IndexOf("&", indice + 1);
                        if (indice2 == -1)
                            indice2 = post.Length; // Hasta el final del petición
                        post = post.Substring(0, indice + 1) + post.Substring(indice2);
                    }
                    else if (post.ToUpper().Contains("GETCAPABILITIES"))
                    {
                        strPeticion = "GETCAPABILITIES";
                    }
                    else if (post.ToUpper().Contains("GETFEATUREINFO"))
                    {
                        strPeticion = "GETFEATUREINFO";

                        if (post.ToUpper().Contains("TEXT/HTML"))
                        {
                            post = post.Replace("text/html", "application/vnd.esri.wms_featureinfo_xml");
                            cambioFormato = true;
                        }
                    }

                    else
                    {
                        strPeticion = "";
                    }

                    break;
            }

            _ComReleaser = new ESRI.ArcGIS.ADF.ComReleaser();
            if (strPeticion.ToUpper() != "CAPABILITIES" && strPeticion.ToUpper() != "GETCAPABILITIES" && strPeticion.ToUpper() != "GETSLD")
            {
                try
                {
                    string traza;
                    IWMSServer m_wmsService = Conectar(out traza);
                    if (m_wmsService == null)
                    {
                        string strError = "<ServiceExceptionReport version=\"1.1.1\"><ServiceException code=\"InvalidFormat\"><![CDATA[ " + traza + "  ]]></ServiceException></ServiceExceptionReport>";
                        Response.Clear();
                        Response.ContentType = "text/xml";
                        Response.Write(strError);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    string strError = "<ServiceExceptionReport version=\"1.1.1\"><ServiceException code=\"InvalidFormat\"><![CDATA[ " + ex.Message + "  ]]></ServiceException></ServiceExceptionReport>";
                    Response.Clear();
                    Response.ContentType = "text/xml";
                    Response.Write(strError);
                    return;
                }

                if (idServicio != "")
                {
                    int num = Convert.ToInt32(idServicio);
                    try
                    {
                        string layerDefs = Request["layerDefs"];
                        string[] colRes;
                        colRes = new string[2];
                        colRes = obtenerTematico(num, layerDefs);

                    }
                    catch (Exception ex)
                    {
                        string strError = "<ServiceExceptionReport version=\"1.1.1\"><ServiceException code=\"InvalidFormat\"><![CDATA[ " + ex.Message + " Al obtener Tematico ]]></ServiceException></ServiceExceptionReport>";
                        Response.Clear();
                        Response.ContentType = "text/xml";
                        Response.Write(strError);
                        return;
                    }
                }
            }

            // create WMS request string (HTTP GET mode)
            switch (strPeticion.ToUpper())
            {

                case "GETSLD":
                    break;
                case "GETSTYLES":   // generamos un XML similar al que devuelve el servidor, a partir de nuestro SLD.xml

                    if (Request.HttpMethod == "GET")
                    {
                        capasMXD = ObtenerCapasMXD(arrLayersReq, capasMXDConfig, capasConfig);
                        req.Set("LAYERS", string.Join(",", capasMXD));
                        strReq = reconstruirStrQueryString(req);
                        strReq = HttpUtility.UrlEncode(strReq, System.Text.Encoding.UTF8).Replace("+", "%20");
                        post = m_wmsService.GetToPost(strReq);
                        //respuesta = m_wmsService.get_Data("", post, out tipoSalida);
                        String respuestaStr = System.Text.Encoding.UTF8.GetString(m_wmsService.get_Data("", post, out tipoSalida));
                        int indiceFinCabecera = respuestaStr.IndexOf("<sld:NamedLayer>");
                        string cabeceraXML = respuestaStr.Substring(0, indiceFinCabecera);
                        if (cabeceraXML.Contains("<!--"))
                        {
                            indiceFinCabecera = respuestaStr.IndexOf("<!--");
                            cabeceraXML = respuestaStr.Substring(0, indiceFinCabecera);
                        }
                        string etiquetaFinal = "</sld:StyledLayerDescriptor>";
                        string[] namedLayersRespuesta = null;

                        if (idServicio != "")
                        {
                            int indiceLabelName = 0;
                            string todosEstilosEncontrados = "<sld:Name>" + layersReq + "</sld:Name>";
                            indiceLabelName = SLDstrEstilos.IndexOf("<sld:Name>" + layersReq + "</sld:Name>", indiceLabelName);

                            while (indiceLabelName != -1)
                            {
                                int indiceLabelUserStyle = SLDstrEstilos.IndexOf("<sld:UserStyle>", indiceLabelName);
                                int indiceLabelNamedLayer = SLDstrEstilos.IndexOf("</sld:NamedLayer>", indiceLabelName);
                                int longitudSeleccion = indiceLabelNamedLayer - indiceLabelUserStyle;
                                todosEstilosEncontrados = todosEstilosEncontrados + SLDstrEstilos.Substring(indiceLabelUserStyle, longitudSeleccion);
                                indiceLabelName = SLDstrEstilos.IndexOf("<sld:Name>" + layersReq + "</sld:Name>", indiceLabelName + 1);
                            }

                            string layerResultadoFinal = "<sld:NamedLayer>\r\n" + todosEstilosEncontrados + "</sld:NamedLayer>\r\n";
                            string ficheroCompleto = cabeceraXML + layerResultadoFinal + etiquetaFinal;

                            Response.Clear();
                            Response.ContentType = tipoSalida;
                            string nombreFicheroSalida = "attachment; filename=Styles" + layersReq + ".xml";
                            Response.AddHeader("content-disposition", nombreFicheroSalida);
                            Response.Write(ficheroCompleto);
                        }
                        else
                        {
                            // 20171128 @ADR: Se habilita la devolución de una respuesta compuesta para mejorar el cumplimiento de las especificaciones WMS y SLD
                            // en lo que respecta al tratamiento de las agrupaciones ficticias. En el caso de los grupos la respuesta proviene de la lectura del nuevo SLDgrupos.xml,
                            // mientras que para las capas individuales se mantiene la devolución de la respuesta del ArcGIS for Server.
                            namedLayersRespuesta = new string[capasMXD.Length];
                            string contenido = "";
                            for (int i = 0; i < capasMXD.Length; i++)
                            {
                                string SLDrespuesta = capasMXD[i].Contains(",") ? SLDstrGrupos : respuestaStr;
                                string[] capasSLD = capasMXD[i].Contains(",") ? arrLayersReq : capasMXD;
                                int indiceInic = SLDrespuesta.IndexOf("<sld:Name>" + capasSLD[i] + "</sld:Name>");
                                int indiceFin = SLDrespuesta.IndexOf("</sld:NamedLayer>", indiceInic);
                                int longitudSeleccion = indiceFin - indiceInic;
                                contenido = SLDrespuesta.Substring(indiceInic, longitudSeleccion);
                                namedLayersRespuesta[i] = "<sld:NamedLayer>\r\n" + contenido + "</sld:NamedLayer>\r\n";
                            }

                            string respuestaCompuesta = cabeceraXML + string.Join("", namedLayersRespuesta) + etiquetaFinal;
                            Response.Clear();
                            Response.ContentType = tipoSalida;
                            string nombreFicheroSalida = "attachment; filename=SLD.xml";
                            Response.AddHeader("content-disposition", nombreFicheroSalida);
                            Response.Write(respuestaCompuesta);
                        }
                    }
                    break;

                case "GETLEGENDGRAPHIC":

                    if (Request.HttpMethod == "GET")
                    {
                        for (int i = 0; i < capasMXDConfig.Length; i++)
                        {
                            if (capasMXDConfig[i].Contains(layerReq) && capasMXDConfig[i].Contains(","))
                            {
                                strReq = HttpUtility.UrlEncode(strReq, System.Text.Encoding.UTF8).Replace("+", "%20");
                                break;
                            }
                            else
                            {
                                capasMXD = ObtenerCapasMXD(arrLayerReq, capasMXDConfig, capasConfig);
                                for (int k = 0; k < capasMXD.Length; k++)
                                {
                                    string[] capasMXDsplit = capasMXD[i].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
                                    req.Set("LAYER", capasMXDsplit[0]);
                                    strReq = reconstruirStrQueryString(req);
                                }
                                strReq = HttpUtility.UrlEncode(strReq, System.Text.Encoding.UTF8).Replace("+", "%20");
                                break;
                            }
                        }
                        post = m_wmsService.GetToPost(strReq);
                    }
                    respuesta = m_wmsService.get_Data("", post, out tipoSalida);

                    Response.Clear();
                    Response.ContentType = tipoSalida;
                    Response.BinaryWrite(respuesta);
                    break;


                //Tanto para GetMap como para GetCapabilities pasamos la petición directamente al servicio

                case "CAPABILITIES":
                case "GETCAPABILITIES":
                    // Obtenemos la versión
                    string strVersion;
                    trazaGeneral = "Es virtual. Obtenemos la version";
                    //Trazar(trazaGeneral);
                    if (strReq.ToUpper().Contains("VERSION="))
                    {
                        int ind1, ind2;
                        ind1 = strReq.ToUpper().IndexOf("VERSION=");
                        ind2 = strReq.ToUpper().IndexOf("&", ind1);
                        if (ind2 == -1)
                        {
                            ind2 = strReq.Length;
                        }
                        strVersion = strReq.Substring(ind1 + 8, ind2 - (ind1 + 8));
                        trazaGeneral = "Version =" + strVersion;
                        //Trazar(trazaGeneral);
                    }
                    else
                    {
                        strVersion = "1.3.0";
                    }

                    System.Xml.XmlDocument m_xmld;
                    string pathFichero;
                    string nombreFichero;
                    nombreFichero = strVersion.Replace(".", "");
                    trazaGeneral = "Nombre Fichero=" + nombreFichero;
                    //Trazar(trazaGeneral);
                    m_xmld = new System.Xml.XmlDocument();

                    //pathBase = this.Page.MapPath(this.AppRelativeTemplateSourceDirectory);
                    try
                    {
                        pathFichero = System.Configuration.ConfigurationManager.AppSettings.Get("PathCapabilities");
                        trazaGeneral = "Intentamos cargar el fichero " + myPath + pathFichero + nombreFichero + ".xml";
                        //Trazar(trazaGeneral);
                        System.IO.StreamReader sr = new System.IO.StreamReader(myPath + pathFichero + nombreFichero + ".xml");
                        string strCapabilities = sr.ReadToEnd();
                        sr.Close();
                        trazaGeneral = "Cargado pero no leido";
                        //Trazar(trazaGeneral);
                        trazaGeneral = "Lo hemos cargado";
                        //Trazar(trazaGeneral);
                        Response.Clear();
                        Response.ContentType = "text/xml";
                        Response.Write(strCapabilities);
                    }
                    catch (Exception ex)
                    {

                        string strError = "<ServiceExceptionReport version=\"1.1.1\"><ServiceException code=\"InvalidFormat\"><![CDATA[ " + ex.Message + "    " + trazaGeneral + " ]]></ServiceException></ServiceExceptionReport>";
                        Response.Clear();
                        Response.ContentType = "text/xml";
                        Response.Write(strError);
                    }

                    break;

                case "GETMAP":
                case "MAP":
                    ImageHandler img = new ImageHandler();
                    if (Request.HttpMethod == "GET")
                    {
                        trazaGeneral = "intentamos arreglar strReq";
                        strReq = HttpUtility.UrlEncode(strReq, Encoding.GetEncoding("utf-8")).Replace("+","%20");
                        trazaGeneral = "arreglamos strReq";
                        //Trazar(trazaGeneral);
                        post = m_wmsService.GetToPost(strReq);
                        trazaGeneral = "GetToPost del GetMap";
                        //Trazar(trazaGeneral);
                    }
                    //Tenemos que obtener las coordenadas para calcular la escala
                    if (System.Configuration.ConfigurationManager.AppSettings.Get("EscalaMinima") != "")
                    {
                        string bbox = Request.QueryString["BBOX"];
                        string[] coords = bbox.Split(",".ToCharArray());
                        string SRS = "";

                        if (Request.QueryString["CRS"] != null)
                            SRS = Request.QueryString["CRS"];
                        else
                            SRS = Request.QueryString["SRS"];

                        if (Request.QueryString["VERSION"] != null)
                        {
                            strVersion = Request.QueryString["VERSION"];
                        }
                        else
                        {
                            strVersion = "1.3.0";
                        }
                        //Mínimas
                        double minX, minY, maxX, maxY;
                        string strDecimales = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                        string strMiles = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator;
                        if (//strVersion == "1.3.0" &&
                            (
                            SRS == "EPSG:4326" || SRS == "EPSG:4230" ||
                            SRS == "EPSG:3034" || SRS == "EPSG:3035" ||
                            SRS == "EPSG:3395" || SRS == "EPSG:4258" ||
                            SRS == "EPSG:4267" || SRS == "EPSG:4269" ||
                            SRS == "EPSG:4324"
                            )
                        )
                        {
                            minX = double.Parse(coords[1].Replace(strMiles, strDecimales));
                            minY = double.Parse(coords[0].Replace(strMiles, strDecimales));
                            maxX = double.Parse(coords[3].Replace(strMiles, strDecimales));
                            maxY = double.Parse(coords[2].Replace(strMiles, strDecimales));
                        }
                        else
                        {
                            minY = double.Parse(coords[1].Replace(strMiles, strDecimales));
                            minX = double.Parse(coords[0].Replace(strMiles, strDecimales));
                            maxY = double.Parse(coords[3].Replace(strMiles, strDecimales));
                            maxX = double.Parse(coords[2].Replace(strMiles, strDecimales));
                        }
                        double width = double.Parse(Request.QueryString["WIDTH"]);
                        double height = double.Parse(Request.QueryString["HEIGHT"]);
                        bool ancho = true;
                        if (height > width)
                            ancho = false;

                        double escala = calcularEscala(SRS, minX, minY, maxX, maxY, width, height, ancho);
                        //Trazar("Escala: " + escala.ToString());
                        double escalaMaxima = double.Parse(System.Configuration.ConfigurationManager.AppSettings.Get("EscalaMinima"));
                        //Trazar("Escala mínima: " + escalaMaxima.ToString());
                        if (escala > escalaMaxima)
                        {
                            tipoSalida = "image/png";
                            respuesta = img.CrearImagenBlanco((int)width, (int)height);
                            respuesta = img.WatermarkImage(respuesta, tipoSalida);
                        }
                        else
                            respuesta = m_wmsService.get_Data("", post, out tipoSalida);
                    }
                    else
                    {

                        respuesta = m_wmsService.get_Data("", post, out tipoSalida);
                    }
                    if (tipoSalida == "application/vnd.ogc.se_xml")
                    {
                        //Se ha producido algún tipo de error...
                        string respStr = System.Text.Encoding.Default.GetString(respuesta);
                        Response.Clear();
                        Response.ContentType = tipoSalida;
                        Response.BinaryWrite(respuesta);
                        break;
                    }
                    string pathImagen = System.Configuration.ConfigurationManager.AppSettings.Get("pathWatermark");
                    string respStr1 = System.Text.Encoding.Default.GetString(respuesta);

                    string tipoMarca = System.Configuration.ConfigurationManager.AppSettings.Get("watermark");
                    byte[] salida;
                    if (tipoMarca == "texto")
                    {
                        salida = img.WatermarkImage(respuesta, tipoSalida);
                        trazaGeneral = "salida tipoMarca texto";
                    }
                    else
                    {
                        salida = img.WatermarkImage2(respuesta, tipoSalida, pathImagen);
                        trazaGeneral = "salida con pathImagen";
                    }
                    //Trazar(trazaGeneral);
                    Response.Clear();
                    Response.ContentType = tipoSalida;
                    Response.BinaryWrite(salida);
                    break;

                //Para GetFeatureInfo debemos realizar el cambio de hoja de estilo
                case "FEATUREINFO":
                case "GETFEATUREINFO":
                    //Debemos asegurarnos de que si la salida se produce en formato html,
                    //proporcionar la salida adecuada previamente formateada.

                    //bool esCapaIdentificable = false;
                    bool esCapaIdentificablePorEscala = false;
                    bool esGrupoIdentificable = false;
                    string[] capasMXDInd = null;
                    string[] titulosMXDInd = null;

                    capasMXD = ObtenerCapasMXD(arrQueryLayersReq, capasMXDConfig, capasConfig);
                    capasIdentificacion = ObtenerCapasIdentificacion(arrQueryLayersReq, capasIdentificacionConfig, capasConfig);
                    titulosMXD = ObtenerTitulosMXD(arrQueryLayersReq, titulosMXDConfig, capasConfig, capasMXDConfig);
                    // 20171122 @ADR: Para obtener capasPeticiones se eliminan las capas no identificables,
                    // por lo que se elimina el control que anteriormente se realizaba a través de bool esCapaIdentificable
                    capasPeticiones = ObtenerCapasPeticion(capasMXD, capasIdentificacion, titulosMXD);

                    for (int i = 0; i < capasPeticiones.Length; i++)
                    {
                        if (capasPeticiones[i].Contains(","))
                        {
                            capasMXDInd = capasMXD[i].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
                            titulosMXDInd = titulosMXD[i].Split(separadorCapas, StringSplitOptions.RemoveEmptyEntries);
                            esGrupoIdentificable = capasIdentificacion[i].IndexOf("1") != -1;
                            break;
                        }
                    }

                    if (esGrupoIdentificable)
                    {
                        // Para mapas con varias capas en los que únicamente será visible una sola capa a una escala determinada
                        //1) Obtenemos la escala mínima de cada Layer
                        IMapServerObjects2 mapServerObjects2 = (IMapServerObjects2)m_MapServer;
                        _ComReleaser.ManageLifetime(mapServerObjects2);
                        IMap map = mapServerObjects2.get_Map(m_MapServer.DefaultMapName);
                        _ComReleaser.ManageLifetime(map);

                        double[] arrayLayersContexto = new double[map.LayerCount];
                        for (int k = 0; k < map.LayerCount; k++)
                        {
                            if (map.get_Layer(k).MinimumScale == 0.0)
                                arrayLayersContexto[k] = escalaMax;
                            else
                                arrayLayersContexto[k] = map.get_Layer(k).MinimumScale; // guardamos las escalas mínimas de cada ILayer
                        }

                        //2) Obtenemos la escala a la que está el mapa en la petición de identificación
                        // Para ello, necesitamos obtener de la petición:
                        // Sistema de Referencia
                        // BBOX: xmin, xmax, ymin, ymax
                        // Ancho y alto
                        // e invocamos al método calcularEscala con esos parámetros

                        double p_minX, p_minY, p_maxX, p_maxY;
                        bool p_ancho = true;
                        double width = double.Parse(Request.QueryString["WIDTH"]);
                        double height = double.Parse(Request.QueryString["HEIGHT"]);
                        if (height > width)
                            p_ancho = false;

                        string strDecimales = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                        string strMiles = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator;

                        string bbox = Request.QueryString["BBOX"];
                        string[] bb = bbox.Split(",".ToCharArray());
                        p_minX = double.Parse(bb[0].Replace(strMiles, strDecimales));
                        p_minY = double.Parse(bb[1].Replace(strMiles, strDecimales));
                        p_maxX = double.Parse(bb[2].Replace(strMiles, strDecimales));
                        p_maxY = double.Parse(bb[3].Replace(strMiles, strDecimales));

                        string p_SRS = "";
                        bool esCRSGeograficas = false;
                        double escalaMapa = 0;
                        if (Request.QueryString["CRS"] != null)
                            p_SRS = Request.QueryString["CRS"];
                        else
                            p_SRS = Request.QueryString["SRS"];

                        if (
                            p_SRS == "EPSG:4326" || p_SRS == "EPSG:4230" ||
                            p_SRS == "EPSG:3034" || p_SRS == "EPSG:3035" ||
                            p_SRS == "EPSG:3395" || p_SRS == "EPSG:4258" ||
                            p_SRS == "EPSG:4267" || p_SRS == "EPSG:4269" ||
                            p_SRS == "EPSG:4324"
                            )
                        {
                            esCRSGeograficas = true;
                        }

                        if (esCRSGeograficas)
                            escalaMapa = calcularEscala(p_SRS, p_minY, p_minX, p_maxY, p_maxX, width, height, p_ancho); // lat, long
                        else
                            escalaMapa = calcularEscala(p_SRS, p_minX, p_minY, p_maxX, p_maxY, width, height, p_ancho); // lon, lat

                        //3) En función de la escala obtenida y los valores de escala mínima de cada capa, podremos saber qué capa
                        // se muestra para poder determinar si es identificable o no.

                        for (int i = 0; i < capasPeticiones.Length; i++)
                        {
                            for (int k = 0; k < map.LayerCount; k++)
                            {
                                if (capasPeticiones[i].Contains(","))
                                {
                                    for (int j = 0; j < capasMXDInd.Length; j++)
                                    {
                                        if (escalaMapa <= arrayLayersContexto[k] && map.get_Layer(k).Name == capasMXDInd[j])
                                        {
                                            esCapaIdentificablePorEscala = true;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    if (escalaMapa <= arrayLayersContexto[k] && map.get_Layer(k).Name == capasMXD[i])
                                    {
                                        esCapaIdentificablePorEscala = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        esCapaIdentificablePorEscala = true;
                    }

                    peticiones = new string[capasPeticiones.Length];

                    for (int j = 0; j < peticiones.Length; j++)
                    {
                        peticiones[j] = strReq.Replace("QUERY_LAYERS=" + queryLayersReq, "QUERY_LAYERS=" + capasPeticiones[j]);
                    }

                    if (cambioFormato)
                    {
                        string[] strValores;
                        string inicio = "<FeatureInfoResponse>";
                        string final = "</FeatureInfoResponse>";
                        strValores = new string[peticiones.Length];
                        bool[] hayResultados = new bool[peticiones.Length];

                        for (int i = 0; i < peticiones.Length; i++)
                        {
                            if (!String.IsNullOrEmpty(peticiones[i])) // Si peticiones es null es que no es identificable
                            {
                                if (!esCapaIdentificablePorEscala)
                                {
                                    Response.Redirect("NoQueryEscala.htm");
                                    break;
                                }

                                peticiones[i] = HttpUtility.UrlEncode(peticiones[i], System.Text.Encoding.UTF8).Replace("+", "%20"); // arg
                                post = m_wmsService.GetToPost(peticiones[i]);
                                respuesta = m_wmsService.get_Data("", post, out tipoSalida);

                                string respStr2 = System.Text.Encoding.GetEncoding("UTF-8").GetString(respuesta);
                                //parseamos a xml
                                System.Xml.XmlDocument xm = new System.Xml.XmlDocument();
                                xm.LoadXml(respStr2);
                                System.Xml.XmlNodeList nNombre = xm.GetElementsByTagName("SERVICEEXCEPTIONREPORT");
                                //Controlamos que no hay un error en la respuesta

                                if (nNombre.Count == 0)
                                {
                                    string nombreLayer = "";
                                    string nombreGroup = "";

                                    if (capasPeticiones[i].Contains(","))
                                    {
                                        string[] groupName = ObtenerTituloGrupo(arrQueryLayersReq, titulosConfig, capasConfig);
                                        nombreGroup = groupName[i];
                                        for (int r = 0; r < titulosMXDInd.Length; r++)
                                        {
                                            //if (titulosMXDInd[r] == capasMXDInd[r])
                                            //{
                                                nombreLayer = titulosMXDInd[r];
                                                break;
                                            //}
                                        }
                                    }
                                    else
                                    {
                                        for (int k = 0; k < capasConfig.Length; k++)
                                        {
                                            if (capasPeticiones[i] == capasConfig[k] || capasPeticiones[i] == "0")
                                            {
                                                nombreLayer = titulosConfig[k];
                                                break;
                                            }
                                        }
                                    }

                                    System.Xml.XmlNodeList campoFeatureInfo = xm.GetElementsByTagName("FeatureInfo");

                                    hayResultados[i] = campoFeatureInfo.Count == 0 ? false : true;
                                    if (!hayResultados[i])
                                    {
                                        break;
                                    }
                                    System.Xml.XmlNodeList featureInfoResp = xm.GetElementsByTagName("FeatureInfoResponse");

                                    if (featureInfoResp.Count != 0)
                                    {
                                        strValores[i] = "";

                                        for (int j = 0; j < featureInfoResp[0].ChildNodes.Count; j++)
                                        {

                                            System.Xml.XmlNode FeatureInfoCol = featureInfoResp[0].ChildNodes[j];

                                            if (capasPeticiones[i].Contains(","))
                                            {
                                                for (int r = 0; r < titulosMXDInd.Length; r++)
                                                {
                                                    //if (FeatureInfoCol.Attributes["layername"].Value == titulosMXDInd[r])
                                                    //{
                                                        nombreLayer = titulosMXDInd[r];
                                                        break;
                                                    //}
                                                }
                                            }
                                            else
                                            {
                                                for (int k = 0; k < capasConfig.Length; k++)
                                                {
                                                    if (FeatureInfoCol.Attributes["layername"].Value == capasConfig[k] || FeatureInfoCol.Attributes["layername"].Value == "0")
                                                    {
                                                        nombreLayer = titulosConfig[k];
                                                        break;
                                                    }
                                                }
                                            }

                                            System.Xml.XmlNodeList FeatureInfo = FeatureInfoCol.ChildNodes;
                                            if (FeatureInfo.Count < 1)
                                            {
                                                continue;
                                            }
                                            strValores[i] += "<LAYER NAME=\"" + nombreLayer + "\" GROUP=\"" + nombreGroup + "\">";
                                            System.Xml.XmlNodeList Fields = FeatureInfo[0].ChildNodes;

                                            for (int ii = 0; ii < FeatureInfo.Count; ii++)
                                            {
                                                strValores[i] += "<FIELDS";


                                                foreach (System.Xml.XmlElement nodo in FeatureInfo[ii].ChildNodes)
                                                {
                                                    System.Xml.XmlNodeList Childs = nodo.ChildNodes;
                                                    string fName = nodo.ChildNodes[0].InnerText.Replace(" ", "");
                                                    if (fName.Equals("OBJECTID") == false)
                                                    {
                                                        fName = fName.Replace("(", "");
                                                        fName = fName.Replace(")", "");
                                                        fName = fName.Replace(":", "");
                                                        fName = fName.Replace("º", "");
                                                        fName = fName.Replace("/", "");
                                                        fName = fName.Replace("?", "");
                                                        fName = fName.Replace("¿", "");
                                                        fName = fName.Replace("€", "");
                                                        fName = fName.Replace("<", "");
                                                        fName = fName.Replace(">", "");
                                                        // 2017-08-28 arg: se reemplaza también el % ya que no
                                                        // se admite us uso como nombre de atributo de un nodo XML.
                                                        fName = fName.Replace("%", "");
                                                        // 2017-08-28 arg: se quitan los números al principio del atributo porque
                                                        // también fallan.
                                                        var pattern = @"^\d*";
                                                        var regex = new Regex(pattern);
                                                        fName = regex.Replace(fName, "");
                                                        string fValue = nodo.ChildNodes[1].InnerText;//.Replace(" ", "");
                                                        // 2017-09-14 anp: Añadido control de flujo para detectar si el valor del atributo es un hipervínculo
                                                        // y tratar el caracter & de manera diferenciada.
                                                        // https://stackoverflow.com/questions/5709232/how-do-i-include-etc-in-xml-attribute-values
                                                        bool esurl = fValue.ToUpper().IndexOf("HTTP://") == 0 || fValue.ToUpper().IndexOf("HTTPS://") == 0;
                                                        if (!esurl)
                                                        {
                                                            fValue = fValue.Replace("Null", "-");
                                                            fValue = fValue.Replace("\"", "'");
                                                            fValue = fValue.Replace("NULL", "-");
                                                            fValue = fValue.Replace("null", "-");
                                                            fValue = fValue.Replace("&", " y ");
                                                            fValue = fValue.Replace("<", "&lt;");
                                                            //fValue = fValue.Replace("*", "");
                                                        }
                                                        else
                                                        {
                                                            fValue = fValue.Replace("&", "&amp;");
                                                        }
                                                        strValores[i] = strValores[i] + " " + fName + "=\"" + fValue + "\"";
                                                    }
                                                }
                                                strValores[i] = strValores[i] + "></FIELDS>";
                                            }
                                            strValores[i] += "</LAYER>";
                                        }
                                    }
                                }
                                else
                                {
                                    Response.Clear();
                                    Response.ContentType = "text/xml";
                                    Response.Write(respStr2);
                                    return;
                                }
                            }
                            else
                            {
                                // Es identificable o no se han encontrado valores
                                Response.Redirect("NoQuery.htm");
                                break;
                            }
                        }

                        if (System.Array.IndexOf(hayResultados, true) < 0)
                        {
                            Response.Redirect("NoData.htm", false);
                        }

                        string strRespuesta;
                        strRespuesta = inicio;
                        for (int j = 0; j < strValores.Length; j++)
                        {
                            strRespuesta = strRespuesta + strValores[j];
                        }
                        strRespuesta = strRespuesta + final;
                        byte[] buffer = Encoding.GetEncoding("UTF-8").GetBytes(strRespuesta);
                        System.IO.MemoryStream input = new System.IO.MemoryStream(buffer);
                        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(input);
                        System.Xml.Xsl.XslCompiledTransform myXslTransform;

                        myXslTransform = new System.Xml.Xsl.XslCompiledTransform();
                        //string myPath = this.Page.MapPath(this.AppRelativeTemplateSourceDirectory);
                        myXslTransform.Load(myPath + "/estilos/wms_featureinfo_html.xsl");
                        System.IO.MemoryStream output = new System.IO.MemoryStream();
                        System.Xml.XmlTextWriter writer = new System.Xml.XmlTextWriter(output, Encoding.GetEncoding("UTF-8"));
                        myXslTransform.Transform(reader, writer);
                        Response.Clear();
                        Response.ContentType = "text/html";
                        Response.Write(Encoding.GetEncoding("UTF-8").GetString(output.ToArray()));
                    }
                    else // Si no hay un cambio de formato (text/plain)
                    {
                        if (!esCapaIdentificablePorEscala)
                        {
                            respuesta = null; // No devuelve nada
                        }
                        else
                        {
                            if (peticiones != null)
                            {
                                // TODO Habría que tratar esto para peticiones múltiples
                                strReq = peticiones[0]; // arg limitado a una consulta
                            }

                            if (Request.HttpMethod == "GET")
                            {
                                strReq = HttpUtility.UrlEncode(strReq, Encoding.GetEncoding("utf-8")).Replace("+", "%20"); // arg
                                post = m_wmsService.GetToPost(strReq);
                            }
                            respuesta = m_wmsService.get_Data("", post, out tipoSalida);
                        }
                        if (respuesta != null)
                        {
                            Response.Clear();
                            Response.ContentType = tipoSalida;
                            Response.BinaryWrite(respuesta);
                        }
                    }

                    break;
                default:
                    //Generamos el mensaje de error y lo mandamos por pantalla
                    string strError2 = "<ServiceExceptionReport version=\"1.1.1\"><ServiceException code=\"InvalidFormat\"><![CDATA[ Petición REQUEST no soportada.  ]]></ServiceException></ServiceExceptionReport>";
                    Response.Clear();
                    Response.ContentType = "text/xml";
                    Response.Write(strError2);
                    break;

            }
        }
        catch (Exception ex)
        {
            string strError = "<ServiceExceptionReport version=\"1.1.1\"><ServiceException code=\"InvalidFormat\"><![CDATA[ " + ex.Message + "    " + trazaGeneral + " "+ ex.ToString() + " ]]></ServiceException></ServiceExceptionReport>";
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.Write(strError);
        }
        finally
        {
            eliminarLayerTematico();
            LiberarConexion();
            //trazaGeneral = "Conexión liberada";
            //Trazar(trazaGeneral);
        }
    }
}
