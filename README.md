#Cambios en la aplicación WMS (entorno ArcGIS 10.0 + ORACLE)

Los cambios en la aplicación WMS que responden al nuevo modelo de Web.Config propuesto para los servicios ya han sido incorporados. Para ello se ha decidido mantener en paralelo en IMSINTDESA tanto la anterior versión como la nueva con el fin de poder ir probando esta última y adaptando las configuraciones de los servicios a la nueva plantilla. La nueva versión se encuentra en la ruta \\imsintdesa\Inetpub\wwwroot\sig2, y en esta carpeta figuran a modo de ejemplo los nuevos ficheros de configuración para los servicios con los que se han realizado las pruebas.

A continuación se listan y comentan las etiquetas que componen el modelo propuesto de Web.Config para IMSINTDESA. En la adaptación de la lógica a la casuística planteada se ha optado por emplear una plantilla común para todos los escenarios y tipos de servicios considerados, por lo que la presencia de las siguientes etiquetas es obligatoria:
````
<configuration>
  <appSettings>
    <add key="ServicioArcGIS" value=""/>
    <add key="IdServicio" value=""/>
    <add key="CapasMXD" value =""/>
    <add key="Capas" value =""/>
    <add key="CapasIdentificables" value=""/>
    <add key="Titulos" value=""/>
    <add key="TitulosMXD" value=""/>
    <add key="Tolerancia" value=""/>
    <add key="EscalaMinima" value=""/>
    <add key="MensajeEscalaMinima" value=""/>
  </appSettings>
  <connectionStrings/>
</configuration>
````
Los valores a cumplimentar deben ajustarse al tipo de servicio y teniendo en cuenta las siguientes premisas:

* Las etiquetas CapasMXD, CapasIdentificables y TitulosMXD guardan relación directa entre sí a través de los índices. CapasMXD hace referencia al nombre de las capas tal y como figuran en el .mxd del servicio. Para los temáticos se seguirá empleando en esta etiqueta el “0” como nombre de la capa. La etiqueta CapasIdentificables puede tener valores de 0 ó 1 en función de si la capa es identificable (“1”) o no es identificable (“0”). En la nueva lógica desaparecen las etiquetas LayerGroup, LayerQuery y LayerQueryGroup, entre otras, y se considera que un grupo es identificable cuando al menos una de sus capas lo es. La etiqueta TitulosMXD tiene por objeto titular individualmente las capas de un grupo en caso de existir. Por defecto, es decir, cuando el valor de la etiqueta TitulosMXD se deja vacío, la aplicación asigna para la salida de los nombres de las capas los que figuran bajo la etiqueta CapasMXD.
* Las etiquetas Capas y Titulos guardan relación directa entre sí a través de los índices. Capas se corresponde con el nombre (<Name>) que figura en el Capabilities, mientras que la nueva etiqueta Titulos se corresponde con el <Title> del Capabilities. 
* IdServicio. En el caso de los servicios temáticos la manera de cubrir esta etiqueta no varía. Para los servicios no temáticos se dejará el valor vacío y sin espacio entre las comillas.
* Tolerancia. El valor por defecto a partir de ahora será 1.
* Las etiquetas EscalaMaxima y MensajeEscalaMaxima pasan a llamarse EscalaMinima y MensajeEscalaMaxima para guardar mayor coherencia con lo que se pretende expresar (menores escalas son aquellas con denominadores mayores). En caso de no ser un servicio con escala mínima, basta con dejar los valores de ambas etiquetas vacíos (sin espacio entre las comillas).
* Las etiquetas PermitirSTYLES y GetStylesFromSLD desaparecen del Config. Los estilos pasan a estar siempre habilitados.
* Los mayores cambios se producen para el control de la identificación de las capas, que pasa a estar basado en las relaciones de índices entre las etiquetas Capas, CapasMXD y CapasIdentificables. A continuación se presentan los Config adaptados a los nuevos requerimientos para la casuística planteada:

###Capa Simple AGUA/AFOROS (temático)
````
<configuration>
  <appSettings>
    <add key="ServicioArcGIS" value="25830/comunTematicosWMS"/>
    <add key="IdServicio" value="72316"/>
    <add key="CapasMXD" value ="0|0"/>
    <add key="Capas" value ="EF.EnvironmentalMonitoringFacilities|Estaciones de Aforo"/>
    <add key="CapasIdentificables" value="1|1"/>
    <add key="Titulos" value="Estaciones de Aforo|Estaciones de Aforo"/>
    <add key="TitulosMXD" value=""/>
    <add key="Tolerancia" value="1"/>
    <add key="EscalaMinima" value=""/>
    <add key="MensajeEscalaMinima" value=""/>
  </appSettings>
  <connectionStrings/>
</configuration>
````

###Capa Simple BIODIVERSIDAD/ENP (ArcGIS Server) SIMPLE
````
<configuration>
  <appSettings>
    <add key="ServicioArcGIS" value="25830/biodivENP"/>
    <add key="IdServicio" value=""/>
    <add key="CapasMXD" value ="Espacios Naturales Protegidos|Espacios Naturales Protegidos"/>
    <add key="Capas" value ="PS.ProtectedSite|Espacios Naturales Protegidos"/>
    <add key="CapasIdentificables" value="1|1"/>
    <add key="Titulos" value="Espacios Naturales Protegidos|Espacios Naturales Protegidos"/>
    <add key="TitulosMXD" value=""/>
    <add key="Tolerancia" value="3"/>
    <add key="EscalaMinima" value=""/>
    <add key="MensajeEscalaMinima" value=""/>
   </appSettings>
  <connectionStrings/>
</configuration>
````

###Capa Simple AGUA/APROVECHAMIENTOS (ArcGIS Server) GRUPO
````
<configuration>
  <appSettings>
    <add key="ServicioArcGIS" value="25830/aguaHidroAprovechamientos"/>
    <add key="IdServicio" value=""/>
    <add key="CapasMXD" value ="Toma,Restitución,Central|Toma,Restitución,Central"/>
    <add key="Capas" value ="EF.EnvironmentalMonitoringFacilities|Aprovechamiento Hidroeléctrico"/>
    <add key="CapasIdentificables" value="1,1,1|1,1,1"/>
    <add key="Titulos" value="Aprovechamiento Hidroeléctrico|Aprovechamiento Hidroeléctrico"/>
    <add key="TitulosMXD" value=""/>
    <add key="Tolerancia" value="3"/>
    <add key="EscalaMinima" value=""/>
    <add key="MensajeEscalaMinima" value=""/>
  </appSettings>
  <connectionStrings/>
</configuration>
````

###Capas múltiples caso hipotético más parecido AGRICULTURA/CaractAgroClimaticas (ArcGIS Server)
````
<configuration>
  <appSettings>
    <add key="ServicioArcGIS" value="25830/agriCAgroClimaticas"/>
    <add key="IdServicio" value=""/>
    <add key="CapasMXD" value ="Tipos de verano de la clasificación climática de Papadakis|Temperatura mínima|Temperatura media anual|Temperatura máxima|Regímenes térmicos según Papadakis|Régimenes de humedad|Pluviometría media anual|Duración media del período seco|Duración media del período frío|Duración media del período cálido|Tipos de invierno de la clasificación climática de Papadakis|Factor R|Evapotranspiración|Índice de potencialidad agrícola de índice Turc en secano|Índice potencialidad agrícola de índice Turc en regadío|Aridez|Clasificación climáticos"/>
    <add key="Capas" value ="Tipos de verano de la clasificación climática de Papadakis|Temperatura mínima|Temperatura media anual|Temperatura máxima|Regímenes térmicos según Papadakis|Régimenes de humedad|Pluviometría media anual|Duración media del período seco|Duración media del período frío|Duración media del período cálido|Tipos de invierno de la clasificación climática de Papadakis|Factor R|Evapotranspiración|Índice de potencialidad agrícola de índice Turc en secano|Índice potencialidad agrícola de índice Turc en regadío|Aridez|Clasificación climáticos"/>
    <add key="CapasIdentificables" value="1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1"/>
    <add key="Titulos" value="Tipos de verano de la clasificación climática de Papadakis|Temperatura mínima|Temperatura media anual|Temperatura máxima|Regímenes térmicos según Papadakis|Régimenes de humedad|Pluviometría media anual|Duración media del período seco|Duración media del período frío|Duración media del período cálido|Tipos de invierno de la clasificación climática de Papadakis|Factor R|Evapotranspiración|Índice de potencialidad agrícola de índice Turc en secano|Índice potencialidad agrícola de índice Turc en regadío|Aridez|Clasificación climáticos"/>
    <add key="TitulosMXD" value=""/>
    <add key="Tolerancia" value="1"/>
    <add key="EscalaMinima" value=""/>
    <add key="MensajeEscalaMinima" value=""/>
  </appSettings>
  <connectionStrings/>
</configuration>
````

###Capas múltiples con grupo (ArcGIS Server) (no hay ejemplo)
````
<configuration>
  <appSettings>
    <add key="ServicioArcGIS" value="25830/ejemplo"/>
    <add key="IdServicio" value=""/>
    <add key="CapasMXD" value="NombreMXDA,NombreMXDB,NombreMXDC| NombreMXDA,NombreMXDB,NombreMXDC|NombreMXD2|NombreMXD2…"/>
    <add key="Capas" value=”NombreInspire1|NombreMapama1|NombreInspire2|NombreMapama2…"/>
    <add key="CapasIdentificables" value="1,1,1|1,1,1|1|1…"/>
    <add key="Titulos" value="Titulo1|Titulo1|Titulo2|Titulo2…"/>
    <add key="TitulosMXD" value="TituloMXDA,TituloMXDB,TituloMXDC | TituloMXDA,TituloMXDB,TituloMXDC|Titulo2|Titulo2…"/>
    <add key="Tolerancia" value="1"/>
    <add key="EscalaMinima" value=""/>
    <add key="MensajeEscalaMinima" value=""/>
  </appSettings>
  <connectionStrings/>
</configuration>
````